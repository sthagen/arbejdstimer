# Security Policy

## Supported Versions

Versions of arbejdstimer currently being supported with security updates.

| Version(s)  | Supported |
|:------------|:----------|
| 2025.1.14   | yes       |
| < 2025.1.14 | no        |

## Reporting a Vulnerability

Please contact the maintainer per stefan@hagen.link
